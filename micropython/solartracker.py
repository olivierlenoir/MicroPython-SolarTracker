# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-11-18 20:48:03
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.17
# Project: Three sensors solar tracker
# Description: Using Raspberry Pi Pico RP2


from machine import Pin, ADC, PWM
from cmath import rect
from math import radians


# Constant
SRV_PIN_A = const(2)
SRV_PIN_B = const(3)
ADC0 = const(26)
ADC1 = const(27)
ADC2 = const(28)

# Set servor-motor
srv_a = PWM(Pin(SRV_PIN_A))
srv_b = PWM(Pin(SRV_PIN_B))

srv_a.freq(50)
srv_b.freq(50)

# Set sensors
sensor_a = ADC(Pin(ADC0))
sensor_b = ADC(Pin(ADC1))
sensor_c = ADC(Pin(ADC2))

# Read sensors
# read value, 0-65535 across voltage range 0.0v - 3.3v
sensor_a.read_u16()
sensor_b.read_u16()
sensor_c.read_u16()

# Barycenter
polar_a = rect(sensor_a.read_u16(), radians(90))
polar_b = rect(sensor_b.read_u16(), radians(210))
polar_c = rect(sensor_a.read_u16(), radians(330))
# Barycenter should be divided by the number of sensors, but it's not mandatory
barycenter = polar_a + polar_b + polar_c
x = barycenter.real
y = barycenter.imag
# use x and y sign to determine direction
